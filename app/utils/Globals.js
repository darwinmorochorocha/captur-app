/**
 * Created by DARWIN MOROCHO on 12/8/2017.
 */


module.exports = {
    LANG: "LANG",//para los ajustes del idioma
    WEB_API: "http://capturtungurahua.org/api/",
    WEB_RES: "http://capturtungurahua.org",
    YANDEX_API_KEY: "trnsl.1.1.20170331T050342Z.3ea967a85121ac1f.4dd5a9489107bc22e3c06259cacc7d8ae5e94e64",

    CANTONES: [
        {id: 1, name: 'Ambato', img: 'ambato', icon: 'c_ambato'},
        {id: 2, name: 'Baños de Agua Santa', img: 'banios', icon: 'c_banios'},
        {id: 3, name: 'Cevallos', img: 'cevallos', icon: 'c_cevallos'},
        {id: 4, name: 'Mocha', img: 'mocha', icon: 'c_mocha'},
        {id: 5, name: 'Patate', img: 'patate', icon: 'c_patate'},
        {id: 6, name: 'Pelileo', img: 'pelileo', icon: 'c_pelileo'},
        {id: 7, name: 'Pillaro', img: 'pillaro', icon: 'c_pillaro'},
        {id: 8, name: 'Quero', img: 'quero', icon: 'c_quero'},
        {id: 9, name: 'Tisaleo', img: 'tisaleo', icon: 'c_tisaleo'},
    ]
};
/**
 * Created by DARWIN MOROCHO on 12/11/2017.
 */

import globals from './Globals';


module.exports.traducir = function (text, tofrom) {
    let url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key=' + globals.YANDEX_API_KEY + '&text=' + text + '&lang=' + tofrom;
    return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            return error;
        });
};

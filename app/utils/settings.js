/**
 * Created by DARWIN MOROCHO on 13/8/2017.
 */
import {
    AsyncStorage,
} from 'react-native';

import GLOBALS from "./Globals"

module.exports.getIdioma = function () {
    return AsyncStorage.getItem(GLOBALS.LANG);
};


module.exports.setIdioma = function (idioma) {
    AsyncStorage.setItem(GLOBALS.LANG, idioma);
};



/**
 *
 * @returns {*|?Item|Promise}
 */
module.exports.getSetting = function (KEY: string): Promise {
    return AsyncStorage.getItem(KEY);
};


/**
 * Created by DARWIN MOROCHO on 11/11/2017.
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    Dimensions, StatusBar,
    TouchableNativeFeedback, Alert
} from 'react-native';

import settings from '../utils/settings';
import {DMStyles as DM} from '../utils/DMStyles'
import {NavigationActions} from 'react-navigation'
import * as Progress from 'react-native-progress';
const win = Dimensions.get('window');
export default  class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            indeterminate: true,
        };
    }


    //verificamos los datos almacenados previamente e inicializamos el progressbar
    componentDidMount() {
        settings.setIdioma('es');
        let progress = 0;
        let loadingInterval = setInterval(() => {
            progress += Math.random() / 5;
            if (progress > 1) {
                progress = 1;
            }
            this.setState({progress});
        }, 200);

        setTimeout(() => {
            settings.getIdioma().then((data) => {
                clearInterval(loadingInterval);
                let page = 'home';
                /*let page = 'idioma';
                 if (data !== null) {
                 page = 'home';
                 }*/
                const resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: page})
                    ]
                });
                this.props.navigation.dispatch(resetAction);

            });
        }, 1000);
    }


    render() {
        return (
            <View style={[DM.flex_1, DM.jc_c, DM.ai_c, {backgroundColor: '#e61650'}]}>
                <Image source={require('../../assets/img/logo.png')} style={[DM.w_360, DM.h_200, DM.ma_b_20]}/>
                <StatusBar backgroundColor='#e61650'/>
                <Progress.Bar color="'rgba(255, 255, 255, 1)" progress={this.state.progress} width={win.width - 50}
                              style={[DM.ma_t_35]}/>
            </View>
        );
    }

}
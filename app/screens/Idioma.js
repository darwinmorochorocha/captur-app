/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    Button,
    TouchableHighlight,
    Dimensions, StatusBar,
    TouchableNativeFeedback, Alert
} from 'react-native';


import {DMStyles as DM} from '../utils/DMStyles';
import Icon from 'react-native-vector-icons/FontAwesome';

import settings from '../utils/settings'
const win = Dimensions.get('window');


export default class Idioma extends Component {

    constructor(props) {
        super(props);

    }


    setIdioma(lang) {
        settings.setIdioma(lang);
        this.props.navigation.navigate('home');
    }


    render() {
        return (
            <View style={[DM.flex_1, DM.jc_c, DM.ai_c, DM.ac_c, {backgroundColor: '#e61650'}]}>
                <StatusBar backgroundColor='#e61650'/>
                <Image source={require('../../assets/img/idioma.png')} style={[DM.w_250, DM.h_250]}/>

                <Text style={[DM.f_bold, DM.f_30, DM.ma_t_20, {color: 'white'}]}>Seleccione su idioma</Text>
                <Text style={[DM.f_20, {color: 'white'}]}>Select your language</Text>

                <View style={[DM.flex_r, DM.ma_t_10]}>
                    <TouchableNativeFeedback
                        onPress={() => this.setIdioma('es')} style={[{borderRadius: 10}]}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View
                            style={[DM.w_120, DM.h_40, DM.ai_c, DM.jc_c, DM.flex_r, DM.ma_10, {backgroundColor: '#ffffff'}]}>
                            <Image source={require('../../assets/img/ic_spain.png')}
                                   style={{flex: 0.4, width: 40, height: 40}}/>
                            <Text style={[{color: '#00897B', flex: 0.6, paddingLeft: 10}]}>ESPAÑOL</Text>
                        </View>
                    </TouchableNativeFeedback>


                    <TouchableNativeFeedback
                        onPress={() => this.setIdioma('en')} style={[{borderRadius: 10}]}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View
                            style={[DM.w_120, DM.h_40, DM.ai_c, DM.jc_c, DM.flex_r, DM.ma_10, {backgroundColor: '#ffffff'}]}>
                            <Image source={require('../../assets/img/ic_english.png')}
                                   style={{flex: 0.4, width: 40, height: 40}}/>
                            <Text style={[{color: '#00897B', flex: 0.6, paddingLeft: 10}]}>ENGLISH</Text>
                        </View>
                    </TouchableNativeFeedback>

                </View>

            </View>
        );
    }


}




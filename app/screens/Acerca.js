/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    Button,
    TouchableHighlight, TouchableOpacity,
    Dimensions, StatusBar,
    TouchableNativeFeedback, Alert,Linking
} from 'react-native';


import {DMStyles as dm} from '../utils/DMStyles';
import Icon from 'react-native-vector-icons/Ionicons';

import settings from '../utils/settings'
const win = Dimensions.get('window');


export default class Acerca extends Component {

    constructor(props) {
        super(props);

    }


    setIdioma(lang) {
        settings.setIdioma(lang);
        this.props.navigation.navigate('home');
    }


    openUrl(url){

        if(url.length<6){
            alert("ERROR:  url no encontrada");
            return;
        }
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                alert("ERROR: no se puede abrir el enlace.");
            }
        });
    }

    render() {
        return (
            <View style={[dm.flex_1, dm.jc_c, dm.ai_c, dm.ac_c, {backgroundColor: '#e61650'}]}>
                <StatusBar backgroundColor='#e61650'/>

                <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                                  style={[dm.p_a, dm.top_5, dm.left_0, dm.pa_20]}>
                    <Icon name="ios-arrow-back-outline" style={[dm.f_40, dm.c_white]}/>
                </TouchableOpacity>

                <Text style={[dm.p_a, dm.top_0, dm.right_0, dm.pa_20, dm.f_40, dm.c_white, {fontFamily: 'lilita'}]}>ACERCA
                    DE</Text>


                <View>
                    <Text
                        style={[dm.c_white, dm.f_25, {fontFamily: 'exo', textAlign: 'center'}]}>CAPTURTUNGURAHUA</Text>
                    <Text style={[dm.c_white, {fontFamily: 'exo', textAlign: 'center'}]}>v1.0</Text>
                    <Text style={[dm.c_white, dm.ma_t_15, {fontFamily: 'exo', textAlign: 'center'}]}>UNIVERSIDAD CENTRAL
                        DEL ECUADOR</Text>
                    <Text style={[dm.c_white, dm.f_10, {fontFamily: 'exo', textAlign: 'center'}]}>CARRERA DE INGENIERÍA
                        EN COMPUTACIÓN GRÁFICA</Text>


                    <Image source={require('../../assets/img/react.png')}
                           style={[dm.w_150, dm.h_150, dm.as_c, dm.ma_t_25]}/>
                    <Text style={[dm.c_white, dm.f_30, {fontFamily: 'exo', textAlign: 'center'}]}>
                        React Native App
                    </Text>
                </View>


                <TouchableOpacity onPress={()=>this.openUrl('http://engisoftware.com')} style={[dm.p_a,dm.bottom_20]}>
                    <Text style={[dm.c_white, dm.f_10, {
                        fontFamily: 'exo',
                        textAlign: 'center',
                        borderBottomColor: '#fff',
                        borderBottomWidth: 1
                    }]}>
                        © ENGISoftware All Rights Reserved
                    </Text>
                </TouchableOpacity>

            </View>
        );
    }


}




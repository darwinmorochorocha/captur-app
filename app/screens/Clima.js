/**
 * Created by DARWIN MOROCHO on 11/11/2017.
 */
import  React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    Button,
    TouchableHighlight, Easing,
    Dimensions, StatusBar, FlatList,TouchableOpacity,
    TouchableNativeFeedback, Alert, ScrollView
} from 'react-native';
import {DMStyles as dm} from '../utils/DMStyles';
import GLOBALS from '../utils/Globals';
import Carousel from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import DropdownAlert from 'react-native-dropdownalert';
import Icon from 'react-native-vector-icons/FontAwesome';
import IonicIcon from 'react-native-vector-icons/Ionicons';
const win = Dimensions.get('window');
import settings from '../utils/settings';

function wp(percentage) {
    const value = (percentage * win.width) / 100;
    return Math.round(value);
}

const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);
const itemWidth = slideWidth + itemHorizontalMargin * 2;
const dias = [
    {en: 'Monday', es: 'Lunes'},
    {en: 'Tuesday', es: 'Martes'},
    {en: 'Wednesday', es: 'Miercoles'},
    {en: 'Thursday', es: 'Jueves'},
    {en: 'Friday', es: 'Viernes'},
    {en: 'Saturday', es: 'Sabado'},
    {en: 'Sunday', es: 'Domingo'},
];

const meses = [
    {en: 'January', es: 'Enero'},
    {en: 'February', es: 'Febrero'},
    {en: 'March', es: 'Marzo'},
    {en: 'April', es: 'Abril'},
    {en: 'May', es: 'Mayo'},
    {en: 'June', es: 'Junio'},
    {en: 'July', es: 'Julio'},
    {en: 'August', es: 'Agosto'},
    {en: 'September', es: 'Septiembre'},
    {en: 'October', es: 'Octubre'},
    {en: 'November', es: 'Noviembre'},
    {en: 'December', es: 'Diciembre'},
];

const tipos_climas_en = [
    'tornado',
    'tropical storm',
    'hurricane',
    'severe thunderstorms',
    'thunderstorms',
    'mixed rain and snow',
    'mixed rain and sleet',
    'mixed snow and sleet',
    'freezing drizzle',
    'drizzle',
    'freezing rain',
    'showers',
    'showers',
    'snow flurries',
    'light snow showers',
    'blowing snow',
    'snow',
    'hail',
    'sleet',
    'dust',
    'foggy',
    'haze',
    'smoky',
    'blustery',
    'windy',
    'cold',
    'cloudy',
    'mostly cloudy (night)',
    'mostly cloudy (day)',
    'partly cloudy (night)',
    'partly cloudy (day)',
    'clear (night)',
    'sunny',
    'fair (night)',
    'fair (day)',
    'mixed rain and hail',
    'hot',
    'isolated thunderstorms',
    'scattered thunderstorms',
    'scattered thunderstorms',
    'scattered showers',
    'heavy snow',
    'scattered snow showers',
    'heavy snow',
    'partly cloudy',
    'thundershowers',
    'snow showers',
    'Scattered Thunderstorms',
];

const tipos_climas_es = [
    'tornado',
    'tormenta tropical',
    'huracán',
    'tormentas eléctricas severas',
    'Tormentas eléctricas',
    'Lluvia y nieve',
    'Lluvia y aguanieve',
    'Nieve mixta y aguanieve',
    'llovizna helada',
    'llovizna',
    'lluvia helada',
    'Chubascos',
    'Chubascos',
    'copos de nieve',
    'Chubascos de nieve liviana',
    'nieve que sopla',
    'nieve',
    'granizo',
    'aguanieve',
    'polvo',
    'nebuloso',
    'neblina',
    'lleno de humo',
    'tempestuoso',
    'Ventoso',
    'frío',
    'nublado',
    'Parcialmente nublado (noche)',
    'Parcialmente nublado (día)',
    'Parcialmente nublado (noche)',
    'Parcialmente nublado (día)',
    'Noche despejada',
    'soleado',
    'Noche clara',
    'Día claro',
    'mezcla de lluvia y granizo',
    'caliente',
    'tormentas aisladas',
    'tormentas eléctricas dispersas',
    'tormentas eléctricas dispersas',
    'Chubascos dispersos',
    'fuertes nevadas',
    'Chubascos de nieve dispersos',
    'fuertes nevadas',
    'parcialmente nublado',
    'Tormentas eléctricas',
    'Chubascos de Nieve',
    'Tormentas eléctricas dispersas',
];

export default class Clima extends Component {


    constructor(props) {
        super(props);
        this.state = {
            canton: 'Ambato',
            climas: [],
            forecasts: [],
            lang: 'es',
            tipos_clima: tipos_climas_es
        }


        settings.getIdioma().then((lang) => {
            this.setState({lang});
            if (lang === 'en') {
                this.setState({tipos_clima: tipos_climas_en});
            }
        });

        GLOBALS.CANTONES.forEach((item) => {
            this.climaProvincia(item);
        });

    }


    climaProvincia(provincia) {
        let url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22' + encodeURI(provincia.name) + '%2C%20ec%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys';

        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                let tmp = this.state.climas.concat([{
                    id: provincia.key,
                    name: provincia.name,
                    condition: responseJson.query.results.channel.item.condition,
                    wind: responseJson.query.results.channel.wind,
                    atmosphere: responseJson.query.results.channel.atmosphere,
                    forecast: responseJson.query.results.channel.item.forecast,
                }]);


                if (tmp.length == 9) {
                    tmp.sort(function (a, b) {
                        if (a.name > b.name) {
                            return 1;
                        }
                        if (a.name < b.name) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    });
                    this.setState({forecasts: tmp[0].forecast});
                }

                this.setState({climas: tmp});
            })
            .catch((error) => {
                //alert(JSON.stringify(error));
            });
    }

    //grdos fareigeit a celcius
    fc(f) {
        return Math.round(( parseFloat(f) - 30) / 1.8);
    }


    getDia(day) {
        switch (day) {
            case "Mon":
                return dias[0][this.state.lang];

            case "Tue":
                return dias[1][this.state.lang];

            case "Wed":
                return dias[2][this.state.lang];

            case "Thu":
                return dias[3][this.state.lang];

            case "Fri":
                return dias[4][this.state.lang];

            case "Sat":
                return dias[5][this.state.lang];

            case "Sun":
                return dias[6][this.state.lang];

            default:
                return dias[0][this.state.lang];
        }
    }


    _renderItemClima({item, index}) {
        return (
            <View style={[dm.flex_c, dm.ai_c, dm.pa_t_50, dm.pa_b_15, {width: itemWidth}]}>
                <Text style={[dm.f_30, dm.c_white, {fontFamily: 'lilita'}]}>{ item.name }</Text>

                <View style={[dm.flex_r, dm.ma_t_10]}>
                    <View style={[dm.jc_c, dm.ai_c,  {flex: 1}]}>
                        <Image source={require('../../assets/img/humidity.png')} style={[dm.w_50, dm.h_50]}/>
                        <Text style={{
                            fontFamily: 'exo',
                            color: '#ffffff',
                            fontSize: 20
                        }}>{item.atmosphere.humidity}%</Text>
                    </View>
                    <View style={[dm.jc_c, dm.ai_c, {flex: 1}]}>
                        <Image source={require('../../assets/img/thermometer.png')} style={[dm.w_50, dm.h_50]}/>
                        <Text style={{
                            fontFamily: 'exo',
                            color: '#ffffff',
                            fontSize: 20
                        }}>{Math.round((parseFloat(item.condition.temp) - 30) / 1.8)}ºC</Text>
                    </View>
                    <View style={[dm.jc_c, dm.ai_c, {flex: 1}]}>
                        <Image source={require('../../assets/img/wind.png')} style={[dm.w_50, dm.h_50]}/>
                        <Text style={{
                            fontFamily: 'exo',
                            color: '#ffffff',
                            fontSize: 20
                        }}>{item.wind.speed}mph</Text>
                    </View>
                </View>

            </View>
        );
    }

    onSlideChange(slideIndex) {
        this.setState({forecasts: this.state.climas[slideIndex].forecast});
    }


    renderForecast = ({item}) => (
        <View style={[dm.w_100p, dm.flex_r, dm.jc_c, {
            backgroundColor: '#e9e9e9',
            borderBottomColor: '#ffffff',
            borderBottomWidth: 2
        }]}>
            <View style={[dm.jc_c, dm.pa_l_40, {flex: 0.7}]}>
                <Text style={[{fontFamily: 'lilita', color: '#0099cc',}, dm.f_25]}>{this.getDia(item.day)}</Text>
                <Text style={[{color: '#0099cc'},dm.f_20]}>{item.date}</Text>
            </View>
            <View style={[{flex: 0.3, backgroundColor: '#0099cc'}, dm.ai_c, dm.pa_hor_10, dm.pa_ver_10]}>
                <Image source={{uri: 'weather_' + item.code}}
                       style={{width: 75, height: 75}}/>
                <Text style={[dm.f_10, {
                    color: '#ffffff',
                    textAlign: 'center'
                }]}>{this.state.tipos_clima[parseInt(item.code)]}</Text>
            </View>
        </View>
    );

    render() {
        return (

            <View style={[dm.flex_1]}>
                <StatusBar translucent={false} backgroundColor='#0099cc'/>

                <View style={[{backgroundColor: '#0099cc'}]}>
                    <Carousel
                        ref={(c) => {
                            this._carousel = c;
                        }}
                        data={this.state.climas}
                        renderItem={this._renderItemClima}
                        inactiveSlideScale={1}
                        sliderWidth={win.width}
                        itemWidth={itemWidth}
                        animationOptions={{
                            easing: Easing.out(Easing.quad),
                            duration: 350,
                            isInteraction: false,
                            useNativeDriver: true
                        }}
                        keyExtractor={(item, index) => index}
                        onSnapToItem={(slideIndex) => this.onSlideChange(slideIndex)}
                    />
                </View>


                <View
                    style={[dm.w_100p, dm.h_50, dm.p_a, dm.top_0, dm.flex_r, dm.jc_c, dm.ai_c, {backgroundColor: '#0099cc'}]}>

                    <View style={[dm.h_50, dm.pa_10, dm.ac_c, dm.ai_c, dm.jc_c, dm.w_50, {flex: 0.1}]}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>

                            <View style={[dm.w_50, dm.h_50, dm.jc_c, dm.ai_c]}>
                                <IonicIcon style={[dm.f_30, {color: '#ffffff'}]}
                                           name={'ios-arrow-back'}/>
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={[dm.pa_hor_10, dm.h_50, dm.jc_c, {flex: 0.9}]}>
                        <Text numberOfLines={1}
                              style={[dm.f_20, dm.pa_l_30, {
                                  color: '#ffffff',
                                  textAlign: 'left',
                                  fontFamily: 'lilita'
                              }]}>CLIMA EN
                            TUNGURAHUA</Text>
                    </View>

                </View>


                <FlatList
                    ref={(c) => {
                        this.forecasts = c;
                    }}
                    data={this.state.forecasts}
                    renderItem={this.renderForecast}
                    keyExtractor={(item, index) => index}
                    style={{marginTop: 2}}
                />

                <DropdownAlert updateStatusBar={false} closeInterval={2000} ref={ref => this.dropdown = ref}
                               onClose={data => this.onClose(data)}/>


            </View>
        );
    }
}


const style = StyleSheet.create({
    nombre: {}
});

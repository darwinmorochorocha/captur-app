import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity, Modal, StatusBar
} from 'react-native';

import MapView from 'react-native-maps';
import GLOBALS from '../utils/Globals';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import {DMStyles as dm} from '../utils/DMStyles';

import MapMarker from '../MapMarker';

const {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = -1.2885431 + 0.03;
const LONGITUDE = -78.6566382 + 0.03;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

function randomColor() {
    return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

class DefaultMarkers extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            markers: [],
            modalVisible: false,
        };
        this.getRadares();
    }

    onMapPress(e) {
        /* this.setState({
         markers: [
         ...this.state.markers,
         {
         coordinate: e.nativeEvent.coordinate,
         key: id++,
         color: randomColor(),
         },
         ],
         });*/
    }


    getRadares() {
        fetch(GLOBALS.WEB_API + 'radares')
            .then((response) => response.json())
            .then((responseJson) => {
                //this.items = this.items.concat(responseJson);
                //this.setState({eventos: responseJson, cargando: false});

                responseJson.forEach(marker => {
                    this.setState({
                        markers: [
                            ...this.state.markers,
                            {
                                coordinate: {
                                    latitude: parseFloat(marker.latitud),
                                    longitude: parseFloat(marker.longitud)
                                },
                                key: id++,
                                color: randomColor(),
                                text: marker.addr
                            },
                        ],
                    });
                });


            })
            .catch((error) => {
                // Alert.alert("Error", JSON.stringify(error));
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar translucent={false} backgroundColor='#E91E63'/>

                <MapView
                    provider={this.props.provider}
                    style={styles.map}
                    showsCompass={false}
                    initialRegion={this.state.region}
                >
                    {this.state.markers.map((marker, index) => (

                        <MapView.Marker key={index}
                                        coordinate={marker.coordinate}>
                            <MapMarker text={marker.text}/>
                        </MapView.Marker>
                    ))}
                </MapView>

                <View style={[dm.p_a, dm.top_5, dm.left_5, dm.right_5, dm.h_50, dm.flex_r, dm.jc_c, {
                    backgroundColor: '#f7f7f7',
                    borderColor: '#e61650', borderWidth: 1, borderRadius: 14,
                }]}>
                    <TouchableOpacity style={[dm.h_50, dm.ai_c, dm.jc_c, {flex: 0.2}]}
                                      onPress={() => this.props.navigation.goBack()}>
                        <IonicIcon style={[{color: '#e61650', fontSize: 30}]} name={'ios-arrow-back-outline'}/>
                    </TouchableOpacity>
                    <View style={[dm.jc_c, dm.h_50, dm.ac_c, dm.ai_c, {flex: 0.6}]}>
                        <Text style={[dm.f_20, {color: '#e61650', fontFamily: 'lilita'}]}>RADARES</Text>
                    </View>

                    <TouchableOpacity style={[dm.h_50, dm.ai_c, dm.jc_c, {flex: 0.2}]}
                                      onPress={() => this.setState({modalVisible: true})}>
                        <IonicIcon style={[{color: '#e61650', fontSize: 30}]} name={'ios-warning-outline'}/>
                    </TouchableOpacity>

                </View>

                <Modal
                    visible={this.state.modalVisible}
                    animationType="slide"
                    transparent={false}
                    onRequestClose={() => {
                        this.setState({modalVisible: false});
                    }}
                >
                    <View style={{marginTop: 22}}>
                        <View style={[dm.w_100p, dm.ai_c, dm.pa_ver_15]}>
                            <IonicIcon style={[{color: '#1cb29c', fontSize: 100}, dm.ma_b_20]}
                                       name={'ios-happy-outline'}/>
                            <Text style={{fontFamily: 'lilita', fontSize: 18}}>MANEJA CON CUIDADO Y EVITA
                                SANCIONES</Text>
                        </View>
                        <Text style={[dm.pa_hor_15,]}>
                            La Camara Provincial de Turismo de Tungurahua informa a sus distinguidos visitantes que en
                            las siguientes zonas marcadas en el mapa existen radares para medir la velocidad. Maneja con
                            cuidado para evitar sanciones y disfrutar tu estadía en Tungurahua.
                        </Text>
                        <View style={[dm.w_100p, dm.ai_c, dm.pa_ver_25]}>
                            <TouchableOpacity
                                onPress={() => this.setState({modalVisible: false})}
                            >
                                <Text style={{fontFamily: 'lilita', fontSize: 18, color: '#1cb29c'}}>ENTENDIDO</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

DefaultMarkers.propTypes = {
    provider: MapView.ProviderPropType,
};

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    latlng: {
        width: 200,
        alignItems: 'stretch',
    },
    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    buttonContainer: {
        position: 'absolute',
        left: 10, top: 10,
        flexDirection: 'row',
        backgroundColor: 'transparent',
    },
});

module.exports = DefaultMarkers;
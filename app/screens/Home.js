/**
 * Created by DARWIN MOROCHO on 11/11/2017.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
    Animated,
    Platform,
    StyleSheet,
    Text, TextInput,
    View,
    Image,
    Button,
    Modal,
    WebView,
    TouchableHighlight, TouchableOpacity,
    Dimensions, StatusBar, FlatList, Easing,
    TouchableNativeFeedback, Alert, ScrollView, NetInfo, BackAndroid, Linking
} from 'react-native';
import {DMStyles as dm} from '../utils/DMStyles';
import Yandex from '../utils/Yandex';
import GLOBALS from '../utils/Globals';
import Carousel, {Pagination} from 'react-native-snap-carousel';

import DropdownAlert from 'react-native-dropdownalert';
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/FontAwesome';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const win = Dimensions.get('window');
import HeaderImageScrollView, {TriggeringView} from 'react-native-image-header-scroll-view';
import * as Progress from 'react-native-progress';
import Drawer from 'react-native-drawer';
import GPSAndroid from '../../android_modules/AndroidModules';

function wp(percentage) {
    let value = (percentage * win.width) / 100;
    return Math.round(value);
}

function hp(percentage) {
    let value = (percentage * win.height) / 100;
    return Math.round(value);
}

const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);
const itemWidth = slideWidth + itemHorizontalMargin * 2;


export default class Home extends Component {


    constructor(props) {
        super(props);

        this.state = {
            canton: 'Ambato',
            cantones: [],
            eventos: [],
            ests: [],
            menuOption: 'turismo',
            cantonOption: 1,
            hayDatos: false,
            cargando: true,
            est: null,
            event: null,
            slider1ActiveSlide: 2,
            modalVisible: false,
            cover: 'http://capturtungurahua.org/img/cover2.jpg',
            carousel: [
                {cover: 'http://capturtungurahua.org/img/cover0.jpg', name: '\nEventos y noticias', type: 'events'},
                {
                    cover: 'http://capturtungurahua.org/img/cover1.jpg',
                    name: 'Gastronomía, bares y discotecas',
                    type: 'gastronomia'
                },
                {
                    cover: 'http://capturtungurahua.org/img/cover2.jpg',
                    name: 'Turismo cultural, natural y religioso',
                    type: 'turismo'
                },
                {
                    cover: 'http://capturtungurahua.org/img/cover3.jpg',
                    name: 'Alojamiento y \n Agencias de viajes',
                    type: 'alojamiento'
                }
            ]
        };


        NetInfo.isConnected.fetch().then(isConnected => {
            if (!isConnected) {

                Alert.alert(
                    'Error de conexión',
                    'No se detecto una conexión a internet',
                    [
                        {
                            text: 'OK', onPress: () => {
                            if (Platform.OS === 'android') {
                                BackAndroid.exitApp();
                            } else {

                            }
                        }
                        },
                    ],
                    {cancelable: false}
                );


            } else {
                //this.getEventos(this.state.cantonOption);
                this.getEstablecimientos(1, 'turismo')
            }


        });


        /* fetch(GLOBALS.WEB_API + 'rest')
         .then((response) => response.json())
         .then((responseJson) => {
         //this.items = this.items.concat(responseJson);
         this.setState({ests: responseJson});
         //alert(JSON.stringify(responseJson));
         })
         .catch((error) => {
         // Alert.alert("Error", JSON.stringify(error));
         });*/


    }


    openUrl(url) {

        if (url.length < 6) {
            alert("ERROR:  url no encontrada");
            return;
        }
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                alert("ERROR: no se puede abrir el enlace.");
            }
        });
    }

    getEventos(cantonId) {
        this.setState({cargando: true});
        //this.dropdown.alertWithType('success', 'Actualizando..', 'Por favor espere');
        fetch(GLOBALS.WEB_API + 'eventos/bycanton/' + cantonId)
            .then((response) => response.json())
            .then((responseJson) => {
                //this.items = this.items.concat(responseJson);
                this.setState({eventos: responseJson, cargando: false});
                //alert(JSON.stringify(responseJson));
                if (responseJson.length > 0) {
                    this.setState({hayDatos: true, cargando: false});
                } else {
                    this.setState({hayDatos: false, cargando: false});
                }
            })
            .catch((error) => {
                // Alert.alert("Error", JSON.stringify(error));
            });

    }


    setMenuOption(menuOption) {
        this.setState({menuOption});
        if (menuOption == 'events') {
            this.getEventos(this.state.cantonOption);
        } else {
            this.getEstablecimientos(this.state.cantonOption, menuOption);
        }
    }


    setCantonOption(cantonOption) {
        this.setState({cantonOption});
        this.setState({cantones: []});
        //this.setState({cantones:GLOBALS.CANTONES});
        this.state
        if (this.state.menuOption == 'events') {
            this.getEventos(cantonOption);
        } else {
            this.getEstablecimientos(cantonOption, this.state.menuOption);
        }
    }

    getEstablecimientos(canton, tipo) {
        //  this.dropdown.alertWithType('success', 'Actualizando..', 'Por favor espere');
        let url = GLOBALS.WEB_API + 'canton/' + canton + '/establecimientos/' + tipo;
        //Alert.alert("Error", url);
        this.setState({cargando: true});
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                //this.items = this.items.concat(responseJson);
                this.setState({ests: responseJson, cargando: false});
                //alert(JSON.stringify(responseJson));
                if (responseJson.length > 0) {
                    this.setState({hayDatos: true, cargando: false});
                } else {
                    this.setState({hayDatos: false, cargando: false});
                }
            })
            .catch((error) => {
                //  Alert.alert("Error", JSON.stringify(error));
            });

    }


    componentDidMount() {

    }


    _renderCanton = ({item}) => (
        <TouchableNativeFeedback
            style={[{borderRadius: 10}]}
            onPress={() => this.setCantonOption(item.id)}
            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
            <View
                style={[dm.w_75, dm.h_75, dm.ai_c, dm.jc_c, {
                    marginHorizontal: 1,
                    marginVertical: 2,
                    borderRadius: 40,
                    backgroundColor: this.state.cantonOption == item.id ? '#fff' : '#e61650'
                }]}>
                <Image source={{uri: 'c_' + item.img}}
                       style={{width: 70, height: 70}}/>
            </View>
        </TouchableNativeFeedback>
    );


    esen(text, index) {
        this.dropdown.alertWithType('info', 'Traduciendo..', 'Por favor espere');
        Yandex.traducir(text, 'es-en').then((result) => {
            var tmp = this.state.eventos;
            tmp[index].descripcion_corta = result.text[0];
            if (result.code == 200) {

                this.setState({eventos: tmp});
            }

        });
    }


    //para dar el stilo al drowdown menu
    _dropdown_adjustFrame(style) {
        // console.log(`frameStyle={width:${style.width}, height:${style.height}, top:${style.top}, left:${style.left}, right:${style.right}}`);
        style.top -= 15;
        style.left += 150;
        return style;
    }


    _renderEvent = ({item, index}) => (
        <TouchableNativeFeedback
            onPress={() => {
                this.setState({event: item, est: null, modalVisible: true});
            }}
            style={[{borderRadius: 10}]}
            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
            <View
                style={[dm.w_98p, dm.flex_r, {
                    marginBottom: 6,
                    backgroundColor: '#f5f5f5',
                    marginLeft: 2,
                    marginRight: 2, elevation: 3

                }]}>
                <Image source={{uri: GLOBALS.WEB_RES + item.imagen}}
                       style={[dm.w_35p, dm.h_15p]}/>

                <View style={[dm.w_65p]}>
                    <View style={[dm.b_secondary, dm.pa_ver_10, dm.pa_hor_10]}>
                        <Text style={[dm.c_white, dm.f_15, {fontFamily: 'lilita'}]}
                              numberOfLines={1}>{item.titulo_evento}</Text>

                        <Text
                            style={[dm.f_10, {
                                color: '#fff',
                                fontFamily: 'light'
                            }]}>{item.fecha_evento + ' - ' + item.hora_evento}</Text>
                    </View>

                    <Text style={[dm.f_12, dm.w_60p, dm.pa_hor_10, {fontFamily: 'light'}]}
                          numberOfLines={3}>{item.descripcion_corta}</Text>
                </View>

            </View>
        </TouchableNativeFeedback>
    );


    //item
    _renderEst = ({item, index}) => (
        <TouchableNativeFeedback
            style={[{borderRadius: 10}]}
            onPress={() => {
                this.setState({event: null, est: item, modalVisible: true});
            }}
            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
            <View
                style={[dm.w_32p, dm.flex_c, {
                    marginBottom: 6,
                    backgroundColor: '#f5f5f5',
                    marginLeft: 3,
                    marginRight: 2,
                }]}>
                <View
                    style={[dm.h_100, dm.jc_c, dm.ai_c]}>
                    <Image source={{uri: GLOBALS.WEB_RES + item.imagen_portada}}
                           style={[dm.p_a, dm.left_0, dm.right_0, dm.top_0, dm.bottom_0]}/>


                    <View style={[dm.p_a, dm.bottom_0, dm.w_32p, dm.ai_c, {
                        paddingHorizontal: 10,
                        backgroundColor: 'rgba(0, 137, 123, 1)'
                    }]}>
                        <Text style={[dm.c_white, dm.f_10]}
                              numberOfLines={1}>{item.nombre_establecimiento}</Text>
                    </View>


                </View>
            </View>
        </TouchableNativeFeedback>
    );


    //cuando se cierra el alert dialog
    onClose(data) {
        // data = {type, title, message, action}
        // action means how the alert was closed.
        // returns: automatic, programmatic, tap, pan or cancel
    }


    //modal
    _renderEstablecimiento = () => {
        if (this.state.est != null) {
            return (<View style={{
                flex: 1,
                justifyContent: 'space-between',
            }}>


                <View style={[dm.w_100p, dm.flex_r, dm.jc_c, dm.ai_c, dm.pa_b_0, {
                    backgroundColor: '#e61650',
                    elevation: 10
                }]}>

                    <TouchableOpacity style={{flex: 0.2}}
                                      onPress={() => {
                                          this.setState({modalVisible: false});
                                      }}>
                        <IonicIcon name="ios-close-outline" style={[dm.pa_hor_20, {color: '#fff', fontSize: 50}]}/>
                    </TouchableOpacity>

                    <Text numberOfLines={1}
                          style={[dm.f_14, dm.pa_l_5, dm.ma_r_10, dm.w_100p, {
                              textAlign: 'center', flex: 0.8,
                              color: '#fff'
                          }]}>{this.state.est.nombre_establecimiento}</Text>

                    <TouchableOpacity style={{flex: 0.2}}
                                      onPress={() => {
                                          if (this.state.est.latitud.length > 1) {
                                              GPSAndroid.showMap(this.state.est.latitud + "," + this.state.est.longitud + "(" + this.state.est.nombre_establecimiento + ")")
                                          } else {
                                              Alert.alert("INFO", "Este establecimiento no tiene definido una dirección.")
                                          }
                                      }}>
                        <MaterialIcon name="google-maps" style={[{color: '#fff', fontSize: 30}]}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{flex: 0.2}}
                                      onPress={() => {
                                          if (this.state.est.latitud.length > 1) {
                                              GPSAndroid.stepToStep(this.state.est.latitud + "," + this.state.est.longitud)
                                          } else {
                                              Alert.alert("INFO", "Este establecimiento no tiene definido una dirección.")
                                          }
                                      }}>
                        <MaterialIcon name="car" style={[{color: '#fff', fontSize: 30}]}/>
                    </TouchableOpacity>

                </View>


                <Image resizeMode='cover' source={{uri: GLOBALS.WEB_RES + this.state.est.imagen_portada}}
                       style={[dm.w_100p, dm.h_30p]}/>
                <WebView style={[dm.ma_b_65, {flex: 1}]} source={{html: this.state.est.descripcion}}/>


                <View style={[dm.p_a, dm.h_60, dm.w_100p, dm.flex_r, dm.ai_c, dm.jc_c, dm.bottom_0, {
                    zIndex: 9,
                    backgroundColor: '#f7f7f7'
                }]}>
                    <TouchableOpacity onPress={() => this.openUrl(this.state.est.pagina_web)}>
                        <View style={[dm.w_60, dm.h_60, dm.ai_c]}>
                            <IonicIcon style={[dm.f_35, {color: '#00897B'}]} name="md-globe"/>
                            <Text style={[dm.f_10]}>Sitio web</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.openUrl(this.state.est.facebook)}>
                        <View style={[dm.w_60, dm.h_60, dm.ai_c]}>
                            <IonicIcon style={[dm.f_35, {color: '#008cc3'}]} name="logo-facebook"/>
                            <Text style={[dm.f_10]}>Facebook</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.openUrl(this.state.est.twitter)}>
                        <View style={[dm.w_60, dm.h_60, dm.ai_c]}>
                            <IonicIcon style={[dm.f_35, {color: '#00a0de'}]} name="logo-twitter"/>
                            <Text style={[dm.f_10]}>Twitter</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.openUrl(this.state.est.youtube)}>
                        <View style={[dm.w_60, dm.h_60, dm.ai_c]}>
                            <IonicIcon style={[dm.f_35, {color: '#de3717'}]} name="logo-youtube"/>
                            <Text style={[dm.f_10]}>Youtube</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </View>);
        }
        else if (this.state.event != null) {
            return (<View style={{
                flex: 1,
                justifyContent: 'space-between',
            }}>


                <View style={[dm.w_100p, dm.flex_r, dm.jc_c, dm.ai_c, dm.pa_b_0, {
                    backgroundColor: '#e61650',
                    elevation: 10
                }]}>

                    <TouchableOpacity style={{flex: 0.2}}
                                      onPress={() => {
                                          this.setState({modalVisible: false});
                                      }}>
                        <IonicIcon name="ios-close-outline" style={[dm.pa_hor_20, {color: '#fff', fontSize: 50}]}/>
                    </TouchableOpacity>

                    <Text numberOfLines={1}
                          style={[dm.f_14, dm.pa_l_5, dm.ma_r_10, dm.w_100p, {
                              textAlign: 'left', flex: 0.8,
                              color: '#fff'
                          }]}>{this.state.event.titulo_evento}</Text>

                </View>


                <Image resizeMode='cover' source={{uri: GLOBALS.WEB_RES + this.state.event.imagen}}
                       style={[dm.w_100p, dm.h_30p]}/>
                <WebView style={[dm.ma_b_65, {flex: 1}]} source={{html: this.state.event.descripcion}}/>


                <View style={[dm.p_a, dm.h_60, dm.pa_hor_10, dm.w_100p, dm.flex_r, dm.ai_c, dm.jc_sb, dm.bottom_0, {
                    zIndex: 9,
                    backgroundColor: '#e61650'
                }]}>
                    <View>
                        <Text style={[dm.c_white]}>
                            {this.state.event.fecha_evento}
                        </Text>
                        <Text style={[dm.c_white]}>
                            {this.state.event.hora_evento}
                        </Text>
                    </View>
                    <View style={[dm.ma_l_10]}>
                        <Text style={[dm.c_white]}>{this.state.event.direccion}</Text>
                    </View>
                </View>


            </View>);
        }
        else {
            return null;
        }
    };

    _renderLIST = () => {
        if (this.state.cargando) {
            return (<View style={[dm.w_100p, dm.jc_c, dm.ai_c, {backgroundColor: '#fff',height:hp(50)}]}>
                <Progress.Circle color='#00897B' borderWidth={2} size={70} indeterminate={true}/>
            </View>);

        }
        else if (!this.state.hayDatos) {
            return (<View style={[dm.w_100p, dm.jc_c, dm.ai_c,   {backgroundColor: '#fff',height:hp(50)}]}>
                <Image source={require('../../assets/img/error.png')} style={[dm.w_80, dm.h_80, dm.ma_b_20]}/>
                <Text style={{fontFamily: 'lilita', textAlign: 'center'}}>NO HAY DATOS PARA MOSTRAR{'\n'} EN ESTE
                    CANTÓN</Text>
            </View>);
        } else if (this.state.menuOption == 'events') {
            return (

                <FlatList
                    ref={(c) => {
                        this.eventos = c;
                    }}
                    data={this.state.eventos}
                    renderItem={this._renderEvent}
                    keyExtractor={(item, index) => index}
                    style={{paddingTop: 2}}
                />
            );
        } else {
            return (
                <FlatList
                    ref={(c) => {
                        this.ests = c;
                    }}
                    data={this.state.ests}
                    renderItem={this._renderEst}
                    keyExtractor={(item, index) => index}
                    numColumns={3}
                    style={{paddingTop: 2}}
                />
            );
        }
    };


    navigate(page) {
        this.props.navigation.navigate(page);
    }


    closeControlPanel = () => {
        this._drawer.close()
    };
    openControlPanel = () => {
        this._drawer.open()
    };

    _renderItemCarousel({item, index}) {
        return (
            <View style={[dm.flex_c, dm.ai_c, dm.pa_t_10, {width: itemWidth}]}>


                <Text style={[dm.f_25, dm.c_white, {fontFamily: 'lilita', textAlign: 'center'}]}>{ item.name }</Text>


            </View>
        );
    }

    onSlideChange(slideIndex) {
        this.setState({
            menuOption: this.state.carousel[slideIndex].name,
            cover: this.state.carousel[slideIndex].cover,
            slider1ActiveSlide: slideIndex
        });
        this.setMenuOption(this.state.carousel[slideIndex].type);
    }


    render() {


        return (

            <Drawer
                ref={(ref) => this._drawer = ref}
                type="displace"
                tapToClose={true}
                openDrawerOffset={100}
                panThreshold={0.2}
                panOpenMask={0.05}
                tweenHandler={Drawer.tweenPresets.parallax}
                content={

                    <View style={[dm.h_100p, dm.bo_white, dm.b_white, dm.pa_t_20]}>

                        <Image source={require('../../assets/img/logo.png')} style={[dm.w_250, dm.h_140, dm.as_c]}/>


                        <TouchableNativeFeedback
                            onPress={() => this.navigate('radares')}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View style={[dm.pa_20, dm.flex_r, dm.jc_c, dm.ac_c, {
                                borderBottomWidth: 2,
                                borderBottomColor: '#f4f4f4'
                            }]}>
                                <IonicIcon style={[dm.f_30, {color: '#000000', flex: 0.2}]}
                                           name={'ios-flag-outline'}/>
                                <Text style={[dm.ma_l_0, dm.jc_c, dm.f_20, {flex: 0.8}]}>Radares</Text>
                            </View>
                        </TouchableNativeFeedback>

                        <TouchableNativeFeedback
                            onPress={() => this.navigate('clima')}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View style={[dm.pa_20, dm.flex_r, dm.jc_c, dm.ac_c, {
                                borderBottomWidth: 2,
                                borderBottomColor: '#f4f4f4'
                            }]}>
                                <IonicIcon style={[dm.f_30, {color: '#000000', flex: 0.2}]}
                                           name={'ios-rainy-outline'}/>
                                <Text style={[dm.ma_l_0, dm.jc_c, dm.f_20, {flex: 0.8}]}>Clima</Text>
                            </View>
                        </TouchableNativeFeedback>

                        <TouchableNativeFeedback
                            onPress={() => this.props.navigation.navigate('acerca')}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View style={[dm.pa_20, dm.flex_r, dm.jc_c, dm.ac_c, {
                                borderBottomWidth: 2,
                                borderBottomColor: '#f4f4f4'
                            }]}>
                                <IonicIcon style={[dm.f_30, {color: '#000000', flex: 0.2}]}
                                           name={'ios-information-circle-outline'}/>
                                <Text style={[dm.ma_l_0, dm.jc_c, dm.f_20, {flex: 0.8}]}>Acerca de</Text>
                            </View>
                        </TouchableNativeFeedback>

                    </View>
                }
            >

                <View style={[dm.flex_1, {backgroundColor: '#ffffff'}]}>


                    <Image style={[dm.w_100p, dm.h_100p, dm.p_a, dm.top_0]}
                           source={{uri: this.state.cover}}/>


                    {/*inicio toolbar*/}
                    <StatusBar translucent backgroundColor='rgba(0,0,0,0)'/>
                    <View
                        style={[dm.w_100p, dm.h_50, dm.p_a, dm.top_20, dm.flex_r, dm.jc_c, dm.ai_c]}>

                        <TouchableNativeFeedback style={{flex: 0.5}}
                                                 onPress={() => this.openControlPanel()}
                                                 background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View style={[dm.h_50, dm.pa_10]}>
                                <IonicIcon style={[dm.f_30, {color: '#ffffff'}]} name={'ios-menu-outline'}/>
                            </View>
                        </TouchableNativeFeedback>


                        <View style={[dm.pa_hor_10, dm.h_50, dm.jc_c, {flex: 9}]}>
                            <Text numberOfLines={1}
                                  style={[dm.f_20, {
                                      color: '#ffffff',
                                      textAlign: 'center',
                                      fontFamily: 'lilita'
                                  }]}>CAPTUR
                                TUNGURAHUA</Text>
                        </View>

                        <TouchableNativeFeedback style={{flex: 0.5}}
                                                 onPress={() => {
                                                     if (Platform.OS === 'android') {
                                                         BackAndroid.exitApp();
                                                     } else {

                                                     }
                                                 }}
                                                 background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View style={[dm.h_50, dm.pa_10]}>
                                <IonicIcon style={[dm.f_30, {color: '#ffffff'}]} name={'ios-exit-outline'}/>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                    {/*fin toolbar*/}


                    {/*inicio cantones*/}
                    <View style={[dm.p_a, dm.top_100]}>


                        <Carousel
                            ref={(c) => {
                                this._carousel = c;
                            }}
                            data={this.state.carousel}
                            renderItem={this._renderItemCarousel}
                            inactiveSlideScale={1}
                            sliderWidth={win.width}
                            itemWidth={itemWidth}
                            firstItem={2}
                            animationOptions={{
                                easing: Easing.out(Easing.quad),
                                duration: 350,
                                isInteraction: false,
                                useNativeDriver: true
                            }}
                            keyExtractor={(item, index) => index}
                            onSnapToItem={(slideIndex) => this.onSlideChange(slideIndex)}
                            slideStyle={{marginTop: hp(14)}}
                        />

                        <Pagination
                            dotsLength={this.state.carousel.length}
                            activeDotIndex={this.state.slider1ActiveSlide}
                            containerStyle={[dm.p_a, dm.ai_c, dm.w_100p, {bottom: -50}]}
                            dotColor={'rgba(255, 255, 255, 0.92)'}
                            dotStyle={{width: 10, height: 10, borderRadius: 5}}
                            inactiveDotColor='#fff'
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                            carouselRef={this._carousel}
                            tappableDots={!!this._carousel}
                        />

                    </View>
                    {/*fin cantones*/}


                    <View style={[dm.jc_fs, dm.ma_b_80, {marginTop: hp(46)}]}>
                        {this._renderLIST()}
                    </View>

                    <View style={[dm.h_80, dm.p_a, dm.bottom_0, dm.b_primary]}>
                        <FlatList
                            ref={(c) => {
                                this._cantones = c;
                            }}
                            horizontal={true}
                            data={GLOBALS.CANTONES}
                            extraData={this.state.cantones}
                            renderItem={this._renderCanton}
                            keyExtractor={(item, index) => index}
                            showsHorizontalScrollIndicator={false}
                            automaticallyAdjustContentInsets={false}
                        />
                    </View>


                    <DropdownAlert updateStatusBar={false} closeInterval={3000} ref={ref => this.dropdown = ref}
                                   onClose={data => this.onClose(data)}/>


                    {/*inicio modalr*/}
                    <Modal
                        visible={this.state.modalVisible}
                        animationType="slide"
                        transparent={false}
                        onRequestClose={() => {
                            this.setState({modalVisible: false});
                        }}
                    >
                        <View style={{
                            flex: 1,
                            justifyContent: 'space-between',
                        }}>
                            {this._renderEstablecimiento()}
                        </View>
                    </Modal>
                    {/*fin modal*/}

                </View>
            </Drawer>


        );
    }
}



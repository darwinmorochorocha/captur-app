import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
    StyleSheet,
    View,
    Text,
} from 'react-native';

const propTypes = {
    text: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string,
    fontSize: PropTypes.number,
};

const defaultProps = {
    fontSize: 8,
    backgroundColor:'#E91E63'
};

class MapMarker extends Component {
    render() {
        const {fontSize, text, backgroundColor} = this.props;
        return (
            <View style={styles.container}>
                <View style={[styles.bubble, {backgroundColor: backgroundColor, borderColor: backgroundColor,}]}>
                    <Text style={[styles.amount, {fontSize, maxWidth: 80, textAlign: 'center'}]}>{text}</Text>
                </View>
                <View style={[styles.arrowBorder, {borderTopColor: backgroundColor}]}/>
                <View style={[styles.arrow, {borderTopColor: backgroundColor}]}/>
            </View>
        );
    }
}

MapMarker.propTypes = propTypes;
MapMarker.defaultProps = defaultProps;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignSelf: 'flex-start',
    },
    bubble: {
        flex: 0,
        flexDirection: 'row',
        alignSelf: 'flex-start',
        padding: 2,
        borderRadius: 3,
        borderWidth: 0.5,
    },
    dollar: {
        color: '#FFFFFF',
        fontSize: 10,
    },
    amount: {
        color: '#FFFFFF',
        fontSize: 13,
    },
    arrow: {
        backgroundColor: 'transparent',
        borderWidth: 4,
        borderColor: 'transparent',
        alignSelf: 'center',
        marginTop: -8,
    },
    arrowBorder: {
        backgroundColor: 'transparent',
        borderWidth: 4,
        borderColor: 'transparent',
        alignSelf: 'center',
        marginTop: 0,
    },
});

export default MapMarker;
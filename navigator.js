/**
 * Created by DARWIN MOROCHO on 11/11/2017.
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    Easing, Animated
} from 'react-native';
import {StackNavigator} from 'react-navigation';

//screens
import splash from './app/screens/Splash';
import idioma from './app/screens/Idioma';
import home from './app/screens/Home';
import clima from './app/screens/Clima';
import radares from './app/screens/Radares';
import acerca from './app/screens/Acerca';

//routes
const Routes = {
    splash: {screen: splash},
    idioma: {screen: idioma},
    home: {screen: home},
    clima: {screen: clima},
    radares: {screen: radares},
    acerca: {screen: acerca},
};

const transitionConfig = () => {
    return {
        transitionSpec: {
            duration: 750,
            easing: Easing.out(Easing.poly(4)),
            timing: Animated.timing,
            useNativeDriver: true,
        },
        screenInterpolator: sceneProps => {
            const {layout, position, scene} = sceneProps

            const thisSceneIndex = scene.index
            const width = layout.initWidth

            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex],
                outputRange: [width, 0],
            })

            return {transform: [{translateX}]}
        },
    }
}


//default screen
const AppNavigator = StackNavigator(
    Routes, {
        initialRouteName: 'splash',
        transitionConfig,
        headerMode: 'none',
    }
);


AppRegistry.registerComponent('CapturTungurahua', () => AppNavigator);


